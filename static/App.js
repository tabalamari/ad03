const contentNode = document.getElementById('contents');
class IssueFilter extends React.Component {
    render() {
        return React.createElement(
            'div',
            null,
            'This is a placeholder for the Issue Filter.'
        );
    }
}
class IssueRow extends React.Component {
    // static get propTypes() {
    //     return {
    //         issue_id: React.PropTypes.number.isRequired,
    //         issue_title: React.PropTypes.string,
    //     };
    // }
    render() {

        const issue = this.props.issue;
        return React.createElement(
            'tr',
            null,
            React.createElement(
                'td',
                null,
                issue.id
            ),
            React.createElement(
                'td',
                null,
                issue.status
            ),
            React.createElement(
                'td',
                null,
                issue.owner
            ),
            React.createElement(
                'td',
                null,
                issue.created.toDateString()
            ),
            React.createElement(
                'td',
                null,
                issue.effort
            ),
            React.createElement(
                'td',
                null,
                issue.completionDate ? issue.completionDate.toDateString() : ''
            ),
            React.createElement(
                'td',
                null,
                issue.title
            )
        );
    }
}

class IssueTable extends React.Component {
    render() {
        const issueRows = this.props.issues.map(issue => React.createElement(IssueRow, { key: issue.id, issue: issue }));
        //в змінну issueRow помісти те, що в тебе в пропсах під ім'ям issues

        // const borderedStyle = { border: "1px solid silver", padding: 6 };
        return (
            // <div>This is a placeholder for a table of Issue.</div>
            React.createElement(
                'table',
                { className: 'bordered-table' },
                React.createElement(
                    'thead',
                    null,
                    React.createElement(
                        'tr',
                        null,
                        React.createElement(
                            'th',
                            null,
                            'Id'
                        ),
                        React.createElement(
                            'th',
                            null,
                            'Status'
                        ),
                        React.createElement(
                            'th',
                            null,
                            'Owner'
                        ),
                        React.createElement(
                            'th',
                            null,
                            'Created'
                        ),
                        React.createElement(
                            'th',
                            null,
                            'Effort'
                        ),
                        React.createElement(
                            'th',
                            null,
                            'Completion Date'
                        ),
                        React.createElement(
                            'th',
                            null,
                            'Title'
                        )
                    )
                ),
                React.createElement(
                    'tbody',
                    null,
                    issueRows
                )
            )
        );
    }
}

class BorderWrap extends React.Component {
    render() {
        const borderedStyle = { border: "1px solid silver", padding: 6 };
        return React.createElement(
            'div',
            { style: borderedStyle },
            this.props.children
        );
    }
}

class IssueAdd extends React.Component {
    constructor() {
        super();
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleSubmit(e) {
        e.preventDefault(); //видаліть e.preventDefault () у handleSubmit (). Що сталося? Ви можете пояснити, чому?
        // Після видалення e.preventDefault () виконується поведінка форми за замовчуванням, тобто реально надіслати форму. Це робить GET (дія за замовчуванням, якщо не вказана) для URL-адреси дії форми, яка збігається з поточною URL-адресою. Таким чином, ефект полягає в оновленні сторінки ще до обробки події.
        let form = document.forms.issueAdd;
        this.props.createIssue({
            owner: form.owner.value,
            title: form.title.value,
            status: 'New',
            created: new Date()
        });
        //clear the form for the next input
        form.owner.value = "";form.title.value = "";
    }
    //Чи вдалося б досягти всього цього, якби ми підтримували стан у IssueTable замість IssueList?
    // Оскільки немає можливості спілкуватися між братами та сестрами (лише батьки до дитини і навпаки), збереження стану в корені ієрархії є найкращою стратегією. Якби ми зберегли стан у IssueTable, зв’язування дії Add означало б виклик функції, яка належить до IssueTable, з IssueAdd. Це і не просто, і не хороша практика.
    render() {
        return React.createElement(
            'div',
            null,
            React.createElement(
                'form',
                { name: 'issueAdd', onSubmit: this.handleSubmit },
                React.createElement('input', { type: 'text', name: 'owner', placeholder: 'Owner' }),
                React.createElement('input', { type: 'text', name: 'title', placeholder: 'Title' }),
                React.createElement(
                    'button',
                    null,
                    'Add'
                )
            )
        );
    }
}

const issues = [{
    id: 1, status: 'Open', owner: 'Ravan',
    created: new Date('2016.08.15'), effort: 5, completionDate: undefined,
    title: 'Error in console when clicking Add'
}, {
    id: 2, status: 'Assigned', owner: 'Eddie',
    created: new Date('2016.08.16'), effort: 14,
    completionDate: new Date('2016.08.30'),
    title: 'Missing bottom border on panel'
}];

class IssueList extends React.Component {
    constructor() {
        //конструктор - це фунцкція яка викликається при с творенні кожного нового екземпляру класу. Якщо конструктор явно не прописаний, викликається конструктор за замовчанням(не явний), який викликає конструкро батьківського класу, якщо батьківський клас є.
        super(); //якщо ми самі ваизначили конструктор, то автоматично конструктор батьківського класу викликаний не буде, треба викликати вручну методом super();
        this.state = { issues: [] };
        this.createIssue = this.createIssue.bind(this);
        // this.createTestIssue = this.createTestIssue.bind(this);
        // setTimeout(this.createTestIssue, 2000);
        // setTimeout(this.createTestIssue.bind(this), 2000);
        //Якщо видалити прив'язку bind (this) у виклику setTimeout (), ви отримаєте помилку, оскільки this.state буде невизначеним (оскільки це тепер об'єкт вікна, а не компонент).
    }
    componentDidMount() {
        this.loadData();
    }
    loadData() {
        setTimeout(() => {
            this.setState({ issues: issues });
        }, 500);
    }
    createIssue(newIssue) {
        const newIssues = this.state.issues.slice();
        newIssue.id = this.state.issues.length + 1;
        newIssues.push(newIssue);
        this.setState({ issues: newIssues });
        // this.state.issues.push(newIssue); // так не можна, це матиме ненавмисні наслідки для деяких методів Життєвого циклу всередині нащадкових компонентів. Особливо в тих методах, які порівнюють старі та нові властивості, ви виявите, що старі та нові властивості однакові.
        // this.setState({ issues: this.state.issues })
    }
    // createTestIssue() {
    //     this.createIssue({
    //         status: 'New', owner: 'Pieta', created: new Date(),
    //         title: 'Completion date should be optional',
    //     });
    // }

    render() {
        return React.createElement(
            'div',
            null,
            React.createElement(
                'h1',
                null,
                'Issue Tracker'
            ),
            React.createElement(IssueFilter, null),
            React.createElement('hr', null),
            React.createElement(IssueTable, { issues: this.state.issues }),
            React.createElement('hr', null),
            React.createElement(IssueAdd, { createIssue: this.createIssue })
        );
    }
}

// const continents = ['Africa', 'America','Asia', 'Australia','Europe'];
// const message = continents.map(c => `Hello ${c}!`).join(' ');

// const component = <p>{message}</p>;
ReactDOM.render(React.createElement(IssueList, null), contentNode); //намалюй перше в другому